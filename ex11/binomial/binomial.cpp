#include <iostream>

// the general factorial
template<int N>
struct Factorial {
  enum { value = N * Factorial<N-1>::value };
};

// the specialization that stops the recursion
template<>
struct Factorial<1> {
  enum { value = 1 };
};

template<int N, int K>
struct BinomialCoefficient {
  enum { value = Factorial<N>::value/(Factorial<K>::value*Factorial<N-K>::value) };
};

// GB: factorial of 0 migh be required given your binomial implementation.


int main() {

  std::cout << "BinomialCoefficient<5,3>::value = " << BinomialCoefficient<5,3>::value << "\n";
  std::cout << "BinomialCoefficient<7,2>::value = " << BinomialCoefficient<7,2>::value << "\n";

}
