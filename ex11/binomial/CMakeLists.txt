cmake_minimum_required(VERSION 2.8)

project(binomial)

add_executable(binomial binomial.cpp)
