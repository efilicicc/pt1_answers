#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>



extern "C" {
  void dsyev_( char* jobz, char* uplo, int* n, double* a, int* lda,
      double* w, double* work, int* lwork, int* info );
}

void print_matrix(const int N, const double* A) {
  for (int i=0; i<N; ++i) {
    for (int j=0; j<N; ++j) {
      std::cout << A[i + j*N] << " ";
    }
    std::cout << '\n';
  }
}

void print_vector(const int N, const double* V) {
  for (int i=0; i<N; ++i) {
    std::cout << V[i] << " ";
    std::cout << '\n';
  }
}

void write_vector(const int N, const double* V, std::ofstream& outfile){
  for (size_t i = 0; i < N; i++) {
    outfile << std::fixed << std::setprecision(8) << V[i] << '\n';
  }
}

int main() {
  double K = 1;
  double m = 1;
  int N = 480;
  double* A = new double[N*N];

  // initialize matrix A
  // A(i,j) = A[i*N + j]  row major
  for (size_t i = 0; i < N; i++) {
    A[i*N + i] = K/m*2;
    A[i*N + i + 1] = K/m*-1; // GB: out of bound for i = N - 1.
    A[(i+1)*N + i] = K/m*-1; // GB: that is not how scalar times a matrix works.
  }

  // // print A;
  // std::cout << "A = \n"; print_matrix(N, A);

  // void dsyev_( char* jobz,
  //                 char* uplo,
  //                 int* n,
  //                 double* a,
  //                 int* lda,
  //              double* w,
  //              double* work,
  //                 int* lwork,
  //                 int* info );

  // Arguments for dsyev_
  int info=0;
  char jobz = 'V';
  char uplo = 'U';
  double* eigens = new double[N];
  int lwork = 4*N;
  double* work = new double[lwork];

  // calculate eiganvalues in vector eigens
  dsyev_(&jobz, &uplo, &N, A, &N, eigens, work, &lwork, &info);

  // calculate eigenfrequencies
  double eigenfrequencies[N];
  for (size_t n = 1; n <= N; n++) {
    eigens[n-1] = std::sqrt(eigens[n-1]);
    eigenfrequencies[n-1] = std::sqrt(K/m*(2-2*std::cos(M_PI/(N+1)*n)));
  }

  // print eigenvalues
  std::cout << "Eigenvalues = \n";
  print_vector(20, eigens);
  // print eigenfrequencies
  std::cout << "Eigenfrequencies = \n";
  print_vector(20, eigenfrequencies);
  // write eigens
  std::ofstream outfile("./results.txt");
  outfile << "Eigenfrequencies from eigenvalues:\n";
  write_vector(N, eigens, outfile);
  outfile << "Eigenfrequencies from formula:\n";
  write_vector(N, eigenfrequencies, outfile);
  outfile << "Eigenvectors from formula row major:\n";
  write_vector(N*N, A, outfile);
  outfile.close();



}
